Candidate Name: Michael Mammoliti
Time: 3.5 hours

Notes:
- it took me days to understand the README, I simplified it a bit.
- I had to install few packages
- I didn't write tests for components as I didn't have time, I wrote tests only for the utilities
- I used npm and not yarn