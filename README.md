# Bulb Coding Challenge

## ====================================
## Dictionary
## ====================================
  MeterReading = number in kWh shown by a meter.
  EnergyUsage = MeterReading(moment2) - MeterReading(moment1)
  EnergyUsage(month M) = MeterReading(last day of month M) - MeterReading(last day of month M-1)

## ====================================
### 1. Cleaning the code (1 hour/45 minutes)
## ====================================
  TODO:
    * Refactor the code to be more React-y
    * Write unit tests (we have added [Enzyme](https://github.com/airbnb/enzyme) and [Jest](https://facebook.github.io/jest/) but feel free to use another framework)
    * Fetch meter readings from [https://storage.googleapis.com/bulb-interview/meterReadings.json](https://storage.googleapis.com/bulb-interview/meterReadings.json)
    * Style the application (if you are familiar with [styled-components](http://styled-components.com/), we would love to see how you use it, if you are not please feel free to use CSS, SASS or LESS. In any cases, do not spend more than 15 minutes on improving the application style)

  DO NOT:
    * Changing how the application is bundled and run
    * Setting up a new linter (we have added eslint with the Airbnb configuration, feel free to tweak it but ideally you wouldn't spend time on this)
    * Using Redux (or any other utility libraries that is not necessary in this very simple case)
    * "Over-styling" the application

## ====================================
### 2. Improving the energy usage calculation
## ====================================
  TODO:
    * fetch **end of month** meter readings here: https://storage.googleapis.com/bulb-interview/meterReadingsReal.json
    * display **monthly** energy usage.
    * Don't try to interpolate at the edges of the dataset.
    * use moment.js

## ====================================
### Utils:
## ====================================
  function isEndOfMonth(mmt) {
    // startOf allows to ignore the time component
    return moment.utc(mmt).startOf('day').isSame(moment.utc(mmt).endOf('month').startOf('day'));
  }

  function getDiffInDays(mmt1, mm2) {
    return mmt1.diff(mm2, 'days');
  }

  function getDaysUntilMonthEnd(mmt) {
    return getDiffInDays(moment.utc(mmt).endOf('month'), mmt);
  }

## ====================================
## Practicalities
## ====================================
  * Please add your name and how much time you spent on the exercise to [NOTES](./NOTES.md). Feel free to add any other notes there too.