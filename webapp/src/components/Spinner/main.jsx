import React from 'react';
import styles from './main.scss';

const Spinner = () => (
  <div className={styles.spinner} />
);

export default Spinner;
