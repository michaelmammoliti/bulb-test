import React from 'react';
import PropTypes from 'prop-types';

import DateUtilities from '../../utils/date';

import styles from './main.scss';

const MeterReadings = ({ data }) => {
  if (!data || !data.length) {
    return null;
  }

  return (
    <div className={styles['meter-readings']}>
      <table>
        <tbody>
          <tr>
            <th><span>Date</span></th>
            <th><span>Reading</span></th>
            <th><span>Unit</span></th>
          </tr>
          {data.map(dataItem => (
            <tr key={dataItem.readingDate} className={styles['meter-readings-row']}>
              <td><span>{DateUtilities.displayDate(dataItem.readingDate)}</span></td>
              <td><span>{dataItem.cumulative}</span></td>
              <td><span>{dataItem.unit}</span></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

MeterReadings.displayName = 'MeterReadings';

MeterReadings.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    readingDate: PropTypes.string,
    cumulative: PropTypes.number,
    unit: PropTypes.string,
  })),
};

MeterReadings.defaultProps = {
  data: [],
};

export default MeterReadings;
