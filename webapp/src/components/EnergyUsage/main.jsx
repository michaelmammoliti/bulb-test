import React from 'react';
import PropTypes from 'prop-types';
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';

import UsageUtilities from '../../utils/usage';

const EnergyUsage = ({ data }) => {
  const estimatedData = UsageUtilities.getEstimationData(data);

  return (
    <div>
      <BarChart width={600} height={400} data={estimatedData}>
        <XAxis dataKey="date" />
        <YAxis dataKey="energyUsage" />
        <CartesianGrid horizontal={false} />
        <Tooltip />
        <Bar dataKey="energyUsage" fill="#03ad54" isAnimationActive />
      </BarChart>
    </div>
  );
};

EnergyUsage.displayName = 'EnergyUsage';

EnergyUsage.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    cumulative: PropTypes.number,
    readingDate: PropTypes.string,
    unit: PropTypes.string,
  })),
};

EnergyUsage.defaultProps = {
  data: [],
};

export default EnergyUsage;
