const fetchMeters = () => (
  fetch('https://storage.googleapis.com/bulb-interview/meterReadingsReal.json')
    .then(response => response.json())
);

export default {
  fetchMeters,
};
