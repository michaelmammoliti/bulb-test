import React from 'react';

import EnergyUsage from '../EnergyUsage/main';
import MeterReadings from '../MeterReadings/main';
import Spinner from '../Spinner/main';

import EnergyUsageApi from './api/energyUsage';

import styles from './main.scss';

class Page extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fetchMeterReadingsRequestStatus: undefined,
      energyUsage: undefined,
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    const success = (response) => {
      this.setState({
        fetchMeterReadingsRequestStatus: 'success',
        energyUsage: response.electricity,
      });
    };

    const fail = () => {
      this.setState({
        fetchMeterReadingsRequestStatus: 'fail',
        energyUsage: undefined,
      });
    };

    // loading
    this.setState({
      fetchMeterReadingsRequestStatus: 'pending',
    });

    EnergyUsageApi.fetchMeters()
      .then(success)
      .catch(fail);
  }

  render() {
    const { fetchMeterReadingsRequestStatus, energyUsage } = this.state;

    return (
      <div className={styles.app}>
        <div className={styles.container}>

          {fetchMeterReadingsRequestStatus === 'pending' &&
            <div className={styles['app-spinner']}>
              <Spinner />
              <p>Loading</p>
            </div>
          }

          {fetchMeterReadingsRequestStatus === 'success' &&
            <div className={styles['app-meters']}>
              <div className={styles.row}>
                <div className={styles.col}>
                  <h2>Energy Usage</h2>
                  <EnergyUsage data={energyUsage} />
                </div>
                <div className={styles.col}>
                  <h2>Meter Readings</h2>
                  <MeterReadings data={energyUsage} />
                </div>
              </div>
            </div>
          }

          {fetchMeterReadingsRequestStatus === 'fail' &&
            <div className={styles['app-fail-message']}>
              <span>Ohoh.. we are unable to fetch data</span>
            </div>
          }
        </div>
      </div>
    );
  }
}

Page.displayName = 'Page';

export default Page;
