/* eslint-env browser */
import React from 'react';
import ReactDOM from 'react-dom';

import 'whatwg-fetch';

import Page from './components/Page/main';

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    React.createElement(Page),
    document.getElementById('mount'),
  );
});
