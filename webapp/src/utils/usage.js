import moment from 'moment';

const getEstimateUsage = (prevMonth, currentMonth, nextMonth) => {
  const prev = moment(prevMonth.readingDate);
  const current = moment(currentMonth.readingDate);
  const next = moment(nextMonth.readingDate);

  const daysUntilEndOfPrevMonth = Math.abs(moment(prev).endOf('month').diff(prev, 'days'));
  const daysUntilEndOfCurrentMonth = Math.abs(moment(current).diff(current.endOf('month'), 'days'));
  const nextMonthDaysAmount = Math.abs(moment(current).endOf('month').diff(next, 'days'));
  const numCurrentDays = Math.abs(moment(current).format('D'));

  const firstHalfMonth = (currentMonth.cumulative - prevMonth.cumulative) / (daysUntilEndOfPrevMonth + numCurrentDays) * numCurrentDays;
  const lastHalfMonth = (nextMonth.cumulative - currentMonth.cumulative) / (daysUntilEndOfCurrentMonth + nextMonthDaysAmount) * daysUntilEndOfCurrentMonth;

  return +(firstHalfMonth + lastHalfMonth).toFixed(2);
};

const getEstimationData = (data) => {
  const newData = [];

  data.forEach((dataItem, index) => {
    const prev = data[index - 1];
    const next = data[index + 1];

    if (!prev || !next) {
      return;
    }

    newData.push({
      ...dataItem,
      date: moment(data[index].readingDate).format('MMM YYYY'),
      energyUsage: getEstimateUsage(prev, dataItem, next),
    });
  });

  return newData;
};

export default {
  getEstimateUsage,
  getEstimationData,
};
