import DateUtilities from './date';

describe('DateUtilities', () => {
  describe('calling "displayDate" and given a string date', () => {
    it('should parse the string and return a date in this format: DD MMM YYY', () => {
      const input = DateUtilities.displayDate('2017-03-31T00:00:00.000Z');

      expect(input).toBe('31 Mar 2017');
    });
  });
});