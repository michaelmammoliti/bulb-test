import UsageUtilities from './usage';

describe('UsageUtilities', () => {
  describe('calling getEstimates with the correct', () => {
    it(`should return an object containing the current estimates and the next's month calculation.`, () => {
      const prev = {
        cumulative: 100,
        readingDate: '2017-03-28T00:00:00.000Z',
        unit: 'kWh',
      };

      const current = {
        cumulative: 500,
        readingDate: '2017-04-28T00:00:00.000Z',
        unit: 'kWh',
      };

      const next = {
        cumulative: 1200,
        readingDate: '2017-05-15T00:00:00.000Z',
        unit: 'kWh',
      };

      const estimate = UsageUtilities.getEstimateUsage(prev, current, next);

      expect(estimate).toEqual(451.14);
    });
  });

  describe('calling getEstimationData with the correct data', () => {
    const mockedData = [
      {
        cumulative: 17600,
        readingDate: '2017-03-31T00:00:00.000Z',
        unit: 'kWh',
      },
      {
        cumulative: 17859,
        readingDate: '2017-04-30T00:00:00.000Z',
        unit: 'kWh',
      },
      {
        cumulative: 18102,
        readingDate: '2017-05-31T00:00:00.000Z',
        unit: 'kWh',
      },
      {
        cumulative: 18290,
        readingDate: '2017-06-30T00:00:00.000Z',
        unit: 'kWh',
      },
    ];

    it(`should extend the objects inside the collection.`, () => {
      const data = UsageUtilities.getEstimationData(mockedData);

      const expectedCollecitonOutput = [
        {
          cumulative: 17859,
          readingDate: '2017-04-30T00:00:00.000Z',
          unit: 'kWh',
          date: 'Apr 2017',
          energyUsage: 259,
        },
        {
          cumulative: 18102,
          readingDate: '2017-05-31T00:00:00.000Z',
          unit: 'kWh',
          date: 'May 2017',
          energyUsage: 243,
        },
      ];

      data.forEach((item) => {
        expect(item).toHaveProperty('date');
        expect(item).toHaveProperty('energyUsage');
      });

      expect(data).toEqual(expectedCollecitonOutput);
    });

    it(`should not include the first item and the last item in the collection.`, () => {
      const data = UsageUtilities.getEstimationData(mockedData);

      expect(data.find(item => item.readingDate === '2017-03-31T00:00:00.000Z')).toBeFalsy();
      expect(data.find(item => item.readingDate === '2017-06-30T00:00:00.000Z')).toBeFalsy();
    });
  });
});