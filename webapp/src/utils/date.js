import moment from 'moment';

const displayDate = str => moment(str).format('DD MMM YYYY');

export default {
  displayDate,
};
